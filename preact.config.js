import netlifyPlugin from 'preact-cli-plugin-netlify';
// import OptimizeCssAssetsPlugin from 'optimize-css-assets-webpack-plugin';
// import Critters from 'critters-webpack-plugin';

export default (config, env, helpers) => {
	netlifyPlugin(config);

	// const extract = (helpers.getPluginsByName(config, 'MiniCssExtractPlugin')[0] || helpers.getPluginsByName(config, 'ExtractTextPlugin')[0]);
	// config.plugins.splice(
	// 	extract && extract.index || config.plugins.length,
	// 	0,
	// 	new OptimizeCssAssetsPlugin({
	// 		cssProcessorPluginOptions: {
	// 			preset: ['default', { discardComments: { removeAll: true } }]
	// 		}
	// 	})
	// );

	const critters = helpers.getPluginsByName(config, 'Critters')[0];
	if (critters) {
		// config.plugins[critters.index] = new Critters({
		// 	preload: 'swap',
		// 	preloadFonts: true
		// });
		//critters.plugin.options.preloadFonts = true;
	}

	// add PRERENDER boolean global
	helpers.getPluginsByName(config, 'DefinePlugin')[0].plugin.definitions.PRERENDER = String(env.ssr===true);

	// Exempt the homepage from lazy-loading
	// helpers.getLoaders(config).forEach( ({ loaders, rule }) => {
	// 	if (loaders && String(loaders).match(/async-component-loader/)) {
	// 		rule.exclude = [
	// 			/(\/|^)routes\/(public|home|error)(\/index)?|\.[tj]sx?$/gi
	// 		].concat(rule.exclude || []);
	// 	}
	// });
};
