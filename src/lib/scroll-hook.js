import { Component } from 'preact';

const HOOKS = [];

function on(fn) {
	if (typeof fn==='function' && HOOKS.push(fn)===1) {
		addEventListener('scroll', invoke);
	}
}

function off(fn) {
	if (typeof fn!=='function') return;
	HOOKS.splice(HOOKS.indexOf(fn)>>>0, 1);
	if (!HOOKS.length) {
		removeEventListener('scroll', invoke);
	}
}

function invoke() {
	let scroller = document.scrollingElement || document.body;
	let e = {
		offset: window.pageYOffset || scroller.scrollTop,
		max: scroller.scrollHeight - window.innerHeight
	};
	for (let i=0; i<HOOKS.length; i++) HOOKS[i](e);
}


export default class ScrollHook extends Component {
	componentDidMount() {
		on(this.props.onScroll);
	}

	componentWillUnmount() {
		off(this.props.onScroll);
	}

	componentWillReceiveProps({ onScroll }) {
		if (onScroll!==this.props.onScroll) {
			off(this.props.onScroll);
			on(onScroll);
		}
	}

	render({ children }) {
		return children && children[0];
	}
}
