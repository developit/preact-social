import { h, Component } from 'preact';

export default class ClickOutsideDetector extends Component {
	componentDidMount() {
		addEventListener('click', this.handleClick);
	}

	componentWillUnmount() {
		removeEventListener('click', this.handleClick);
	}

	handleClick = e => {
		if (!this.base.contains(e.target)) {
			this.props.onClick(e);
		}
	};

	render({ children }) {
		return children[0];
	}
}
