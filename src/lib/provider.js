import { Component } from 'preact';

export default class Provider extends Component {
	getChildContext() {
		const context = {};
		for (let i in this.props) {
			if (i!=='children') {
				context[i] = this.props[i];
			}
		}
		return context;
	}

	render(props) {
		return props.children[0];
	}
}
