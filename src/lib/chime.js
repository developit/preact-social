import mitt from 'mitt';


const ORIGIN = 'https://chimeline.herokuapp.com/public';


export default function chimeClient({ config, store }) {
	let api = mitt(),
		openRequests = [],
		cache = {};

	api.store = store;

	api.openRequests = openRequests;

	api.init = () => {
		let { token } = store.getState();

		if (token) {
			store.setState({ loggedin: true, authfailed: false });
			api.emit('login');
			return api.user.me().then(Boolean).catch( err => {
				store.setState({ loggedin: false, authfailed: true });
				api.emit('authfail');
				return false;
			});
		}

		return Promise.resolve(false);

	};

	api.on('login', () => {
		if (!store.getState().user) api.user.me();
	});

	function request(url, opts={}) {
		let e = { req: { ...opts, url } };
		before(e);
		url = e.req.url;
		if (!url.match(/^(https?:)?\/\//)) url = (config.origin || ORIGIN) + url;
		url = applyQuery(url, e.req.query);
		let { req } = e;
		if (req.serializedBody) req = { ...req, body: req.serializedBody };

		let isPure = config.cache===true && isMethodPure(req.method);

		if (req.cache!==false && isPure && typeof cache[req.url]!=='undefined') {
			return cache[req.url];
		}

		let options = { ...req };
		if (req.cache===false) options.cache = 'no-cache';
		else delete options.cache;
		let ret = fetch(url, options).then(asJson).then( data => {
			let { response } = data;
			e.res = req.res = ret.res = {
				status: response.status,
				headers: {},
				data
			};
			after(e);
			let index = openRequests.indexOf(ret);
			if (~index) openRequests.splice(index, 1);
			let loading = openRequests.length;
			store.setState({ loading });
			if (!loading) api.emit('loadend');
			if (!e.res.status || e.res.status>=400) {
				let error = Error(e.res.data.message || `Error ${e.res.status}`);
				ret.syncValue = error;
				return Promise.reject(error);
			}
			ret.syncValue = e.res.data;
			return e.res.data;
		});

		ret.req = req;
		let loading = openRequests.push(ret);
		store.setState({ loading });
		if (loading===1) api.emit('loadstart');

		if (isPure) {
			cache[req.url] = ret;
		}

		return ret;
	}

	api.request = request;

	function before({ req }) {
		let { token, username } = store.getState();
		req.url = req.url.replace(/\/me(\/|$)/, '/'+enc(username)+'$1');
		let headers = req.headers || (req.headers = {});
		if (token && !headers.Authorization) {
			headers.Authorization = `Bearer ${token}`;
		}
		api.emit('req', { req });
		if (req.body && typeof req.body==='object' && !(req.body instanceof FormData)) {
			headers['Content-Type'] = 'application/json';
			req.serializedBody = JSON.stringify(req.body);
		}
	}

	function after({ req, res }) {
		let token = res.data && res.data.token || res.data.access_token;
		if (req.url==='/account/authorize' && token && token!==store.getState().token) {
			let { password, ...credentials } = req.body;
			store.setState({ loggedin: true, token, ...credentials });
			api.emit('login', store.getState());
		}
		api.emit('res', { req, res });
	}

	let method = (method, url, after) => (...args) => {
		let named = {};
		let resolved = url.replace(/(^|\/):(\w+)(\/|$)/g, (s, a, key, b) => {
			let arg = named[key] || (named[key] = args.shift());
			return a + enc(arg) + b;
		});
		let body = args.pop(),
			callback = args.pop();
		if (!callback && typeof body==='function') {
			callback = body;
			body = null;
		}
		let opts = { method };
		if (isMethodPure(method)) {
			Object.assign(opts, body);
		}
		else {
			opts.body = body;
		}
		if (callback && typeof callback==='object') {
			Object.assign(opts, callback);
		}
		let ret = request(resolved, opts);
		if (after) ret = ret.then(after);
		if (callback) toCallback(ret, callback);
		return ret;
	};

	api.isLoggedIn = () => !!store.getState().token;

	api.currentUser = () => store.getState().user;

	api.isLoading = () => openRequests>0;

	/** { offset } */
	api.timeline = method('get', '/timeline');

	api.publicTimeline = method('get', '/public');

	api.notifications = method('get', '/notifications');

	// api.search = (q, opts) => {
	// 	q = q.replace(/^#/,'');
	// 	return api.publicTimeline(opts).then( posts => posts.filter( post => post.body.match(q) ) );
	// };
	api.search = (q, opts) => {
		let hashtag = q.replace(/^#/,'');
		return api.hashtag(hashtag, opts).catch( err => {
			if (String(err).match(/no post exists/i)) return [];
			return err;
		});
	};

	api.hashtag = method('get', '/hashtag/:hashtag');

	api.account = {
		authorize({ username, password }) {
			return request('/account/authorize', { method: 'post', body: {
				username,
				password,
				description: config.clientDescription || `chime.js @ ${location.origin}`,
				access_level: config.accessLevel || 2
			} });
		},

		/** { username, password, email, name } */
		register(info) {
			return request('/account/register', { method: 'post', body: info })
				.then( () => api.account.authorize(info) );
		},

		updateProfile: method('post', '/account/profile')
	};

	api.refresh = () => {
		api.emit('refresh');
	};

	function fireRefresh(d) {
		api.refresh();
		return d;
	}

	api.user = {

		/** Fetch the stream for a given user (by id) */
		profile: method('get', '/users/:username'),

		posts: method('get', '/users/:username/posts'),

		comments: method('get', '/users/:username/comments'),

		followers: method('get', '/users/:username/followers'),

		following: method('get', '/users/:username/following'),

		// me: method('get', '/users/me'),
		me: opts => request(`/users/${enc(store.getState().username)}`, opts)
			.then( user => (store.setState({ user }), user)),

		follow: method('put', '/users/:username/follow'),

		unfollow: method('delete', '/users/:username/follow')
	};


	api.posts = {
		// get: method('get', '/posts/:id'),
		get(id, withComments=false, opts) {
			if (typeof withComments==='object' && withComments) {
				opts = withComments;
				withComments = opts.withComments;
			}
			return request(`/posts/${enc(id)}${withComments?'/with_comments':''}`, opts);
		},

		/** Publish a text post */
		create: method('post', '/posts/create', fireRefresh),

		/** Delete a post. */
		delete: method('delete', '/posts/:id'),

		like: method('put', '/posts/:id/like'),

		unlike: method('delete', '/posts/:id/like')
	};


	api.comments = {
		get: method('get', '/comments/:id'),

		/** Publish a text post */
		create: method('post', '/posts/:postId/comment'),

		/** Delete a comment. */
		delete: method('delete', '/comments/:id'),

		like: method('put', '/comments/:id/like'),

		unlike: method('delete', '/comments/:id/like')
	};


	/** Like a post */
	api.like = api.posts.like;

	/** Un-like a post */
	api.unlike = api.posts.unlike;

	/** Upload image to Imgur. */
	api.uploadImage = image => {
		let body = new FormData();
		body.append('type', 'file');
		body.append('image', image);
		return request('https://api.imgur.com/3/image', {
			method: 'POST',
			headers: { Authorization: `Client-ID ${config.imgurKey}` },
			body
		}).then( data => {
			data = data && data.data || data;
			if (!data) throw 'Invalid Response';
			let url = data.link || `https://i.imgur.com/${data.id}.png`;
			if (typeof url==='string') url = url.replace(/^http:\/\//g, 'https://');
			return { url, ...data };
		});
	};

	api.deleteImage = image => {
		if (typeof image==='object' && image.deletehash) image = image.deletehash;
		if (!image) return Promise.reject('Unknown image');
		return request(`https://api.imgur.com/3/image/${enc(image)}`, {
			method: 'DELETE',
			headers: { Authorization: `Client-ID ${config.imgurKey}` }
		});
	};


	api.plugin = fn => fn(api, { config, request });

	api.init();

	return api;
}


function applyQuery(url, query) {
	for (let key in Object(query)) if (query.hasOwnProperty(key)) {
		url += (~url.indexOf('?')?'&':'?') + enc(key) + '=' + enc(query[key]);
	}
	return url;
}


function asJson(r) {
	let isJson = String(r.headers.get('content-type')).match(/json/);
	return (isJson ? r.json() : r.text()).then( data => {
		if (!isJson) data = { message: data };
		if (!data) data = {};
		Object.defineProperty(data, 'response', {
			configurable: true,
			enumerable: false,
			value: r
		});
		return data;
	});
}


function toCallback(promise, callback) {
	promise.then(data => callback(null, data), callback);
}


function isMethodPure(method) {
	return /^(GET|HEAD|)$/i.test(method || '');
}


const enc = encodeURIComponent;
