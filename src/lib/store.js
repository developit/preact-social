import createStore from 'unistore';

export default ({ id, config }) => {
	let store = createStore();

	id = id || config.storeId || 'chimedata';

	if (!PRERENDER) {
		// populate innitially from localStorage
		try {
			store.setState(JSON.parse(localStorage.getItem(id)) || {});
		} catch (e) {}

		// save to localStorage after writes
		let timer;
		store.subscribe(data => {
			clearTimeout(timer);
			timer = setTimeout(save, 500, data);
		});
		function save(data) {
			localStorage.setItem(id, JSON.stringify(data));
		}
	}

	return store;
};
