import { h, Component } from 'preact';
import distanceInWordsToNow from 'date-fns/distance_in_words_to_now';

const TIMES = [];

const INTERVAL = 60 * 1000;

let timer;

function updateTimes() {
	let update = { now: Date.now() };
	TIMES.forEach( time => {
		time.setState(update);
	});
}

export default class RelativeTime extends Component {
	componentDidMount() {
		if (TIMES.push(this) === 1) timer = setInterval(updateTimes, INTERVAL);
	}
	componentWillUnmount() {
		let index = TIMES.indexOf(this);
		if (~index) TIMES.splice(index, 1);
		if (TIMES.length === 0) clearInterval(timer);
	}
	render({ time }) {
		let local = new Date(time.replace(' ','T')+'.000Z'),
			relative = distanceInWordsToNow(local, { addSuffix: true });
		return <time title={local.toLocaleString()}>{relative}</time>;
	}
}
