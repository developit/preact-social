import { h, Component } from 'preact';
import delve from 'dlv';
import shallowEqual from './shallow-equal';

const CACHE = {};

const none = () => {};

export function select(properties) {
	if (typeof properties==='string') properties = properties.split(',');
	return state => {
		let selected = {};
		for (let i=0; i<properties.length; i++) {
			selected[properties[i]] = state[properties[i]];
		}
		return selected;
	};
}

export default (mapToProps, mapChimeToProps=none) => {
	if (mapChimeToProps && typeof mapChimeToProps !== 'function') mapChimeToProps = select(mapChimeToProps);
	return Child => class ChimeData extends Component {
		chimeMapping = mapChimeToProps(this.context.chime, this.props);

		shouldComponentUpdate(props, state) {
			return !shallowEqual(props, this.props) || !shallowEqual(state, this.state);
		}

		refresh = () => {
			if (typeof this.props.refresh==='function') {
				this.props.refresh();
			}
			else {
				this.invoke({ cache: false });
			}
		};

		invoke = opts => {
			let { chime } = this.context,
				isFunc = typeof mapToProps==='function',
				mapping = isFunc ? mapToProps(this.props) : mapToProps,
				keys = [];
			for (let prop in mapping) if (mapping.hasOwnProperty(prop)) {
				let path = mapping[prop],
					args = [];
				if (Array.isArray(path)) {
					args = path.slice(1);
					path = path[0];
				}
				// for simple object mappings, 2..n are keys in props.
				if (!isFunc) args = args.map( p => this.props[p] );

				// let key = [path, ...args].join('|');
				let key = JSON.stringify([path, ...args]);
				keys.push(key);
				if (opts && opts.keysOnly) continue;

				if (opts && opts.cacheOnly) {
					this.setState({ [prop]: CACHE[key] });
					continue;
				}

				let fn = delve(chime, path);
				if (!fn) throw Error(`chime.${path} not found.`);
				if (opts && opts.cache===false) args.push({ cache: false });
				let p = fn(...args);

				// magically re-render for async values:
				if (p && p.then) {
					p.then( data => {
						CACHE[key] = data;
						this.setState({ [prop]: data });
					});

					// if we got a Promise but there's a cached value, use that until the new value comes in:
					if (typeof CACHE[key]!=='undefined') {
						p = CACHE[key];
					}
					else {
						continue;
					}
				}
				else {
					this.setState({ [prop]: p });
				}
			}

			return this.keys = keys;
		};

		componentWillMount() {
			this.invoke({ cacheOnly: true });
			this.invoke();
		}

		componentDidMount() {
			this.context.chime.on('refresh', this.refresh);
		}

		componentWillUnmount() {
			this.context.chime.off('refresh', this.refresh);
		}

		componentDidUpdate() {
			if (this.keys && this.keys.join('\n')!==this.invoke({ keysOnly: true }).join('\n')) {
				this.invoke();
			}
		}

		render(props, state) {
			return <Child refresh={this.refresh} {...this.chimeMapping} {...props} {...state} />;
		}
	};
};
