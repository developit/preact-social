import { Component } from 'preact';
import cx from 'clsx';
import chime from '../lib/chime-connect';
import RelativeTime from '../lib/relative-time';
import Reply from './reply';
import EmptyPost from './empty-post';
import ClickOutsideDetector from '../lib/click-outside-detector';
import parseMessage from '../lib/parse-message';


export const GetPost = chime({
	post: ['posts.get', 'id', 'showComments']
})( props =>
	props.post ? <Post {...props} refresh={props.refresh} /> : <EmptyPost />
);


export const GetPostForComment = chime({
	comment: ['comments.get', 'id']
})( ({ comment, ...props }) => (
	comment ? (
		<GetPost
			{...props}
			id={comment.post_id}
			showComments
			selectedComment={comment.id}
		/>
	) : <EmptyPost />
));


export const GetComment = chime({
	post: ['comments.get', 'id']
})( props =>
	props.post ? <Post {...props} refresh={props.refresh} /> : <EmptyPost />
);


const FRESH_STATE = {
	// true shows inline reply field
	reply: false,
	// optimistic local like state is handled by setting this to a boolean
	liked: null
};


@chime({
	currentUser: 'currentUser'
})
export default class Post extends Component {
	state = { ...FRESH_STATE };

	/** If we get a new post, wipe state */
	componentWillReceiveProps({ post }) {
		if (this.props.post && post && post.id!==this.props.post.id) {
			this.setState(FRESH_STATE);
		}
	}

	/** Like the post. @todo support comments */
	like = () => {
		let { post, onLike, onUnlike } = this.props,
			{ id, is_liked } = post;
		this.setState({ liked: !is_liked });
		this.callMethod(is_liked ? 'unlike' : 'like', id).then( () => {
			post.is_liked = !is_liked;
			post.likes_count += is_liked ? -1 : 1;
			this.setState({ liked: null });
		});
	};

	callMethod(method, ...args) {
		let type = typeof this.props.post.post_id==='undefined' ? 'posts' : 'comments';
		return this.context.chime[type][method](...args);
	}

	/** Delete the post. @todo support comments */
	delete = () => {
		let { post, onDelete, refresh } = this.props;
		this.callMethod('delete', post.id).then( () => {
			this.setState({ deleted: true });
			if (typeof refresh==='function') refresh();
		});
	};

	/** Inline reply toggle */
	reply = () => this.setState({ reply: true });
	cancelReply = () => this.setState({ reply: false });

	/** Post menu toggle */
	toggleMenu = () => this.setState({ menuOpen: !this.state.menuOpen });
	closeMenu = () => this.setState({ menuOpen: false });

	replied = () => {
		this.cancelReply();
		this.props.refresh();
	};

	render({ refresh, post, user, selected, isModal, showComments, selectedComment, onReply, currentUser }, { reply, liked, deleted, menuOpen }) {
		// locally deleted post is just a noop
		if (deleted) return null;

		if (selectedComment!=null && showComments!==false) showComments = true;

		user = post.user = post.user || user;

		let type = typeof post.post_id==='undefined' ? 'post' : 'comment';

		// parses post body+entities into JSX children
		let parsed = parseMessage(post);

		// if it's the user's post, show post tools
		let isMine = currentUser && currentUser.username===user.username;

		return (
			<div class={cx('post', 'is-'+type, selected && 'is-selected')}>
				<article class="media">
					<figure class="media-left">
						<a href={'/profile/'+encodeURIComponent(user.username)}>
							<p class="image is-48x48 is-avatar" style={{ backgroundImage: `url('${user.avatar_url}')` }} />
						</a>
					</figure>
					<div class="media-content">
						<div class="content">
							<div>
								<a href={'/profile/'+encodeURIComponent(user.username)}>{user.name}</a>
							</div>
							<div class="post-text">
								<span style={{
									fontSize: type==='post' ? `${100 + (1 - post.body.length/200) * 35}%` : null
								}}>
									{parsed}
								</span>
							</div>
							<div>
								<button class={cx(
									'button is-inverted',
									(liked!==null ? liked : post.is_liked) ? 'is-danger' : 'is-primary',
									!currentUser && 'is-disabled'
								)} onClick={this.like}>
									<span class="icon is-small">
										<i class="fa fa-heart" />
									</span>
									<span class="count is-like-count">{post.likes_count + (liked!==null ? (liked ? 1 : -1) : 0)}</span>
								</button>
								<button class={cx('button is-inverted is-primary', !currentUser && 'is-disabled')} onClick={type==='comment' ? onReply : this.reply}>
									<span class="icon is-small">
										<i class="fa fa-reply" />
									</span>
									<span class="count is-comment-count">{post.comments_count}</span>
								</button>
								<a class="button is-link is-text" href={location.pathname+'?'+type+'='+post.id} style={{ opacity: .5 }}>
									<RelativeTime time={post.created_at} />
								</a>
							</div>
						</div>
					</div>

					<ClickOutsideDetector onClick={this.closeMenu}>
						<div class="media-right">
							<button class={cx('button', 'is-link', 'is-text', menuOpen && 'is-active')} onClick={this.toggleMenu}>
								<span class="icon">
									<i class="fa fa-angle-down"></i>
								</span>
							</button>

							{ menuOpen && (
								<PostMenu
									post={post}
									user={user}
									currentUser={currentUser}
									isMine={isMine}
									onDelete={this.delete}
								/>
							) }
						</div>
					</ClickOutsideDetector>
				</article>

				{ showComments && post.comments && (
					<div key="comments" class="post-comments">
						{ post.comments.map( comment => (
							<Post
								post={comment}
								isModal={isModal}
								currentUser={currentUser}
								selected={comment.id==selectedComment}
								refresh={refresh}
								onReply={this.reply}
							/>
						) )}
					</div>
				) }

				{ reply && (
					<Reply
						key="reply"
						modal={!isModal}
						post={post}
						onAdd={this.replied}
						onCancel={this.cancelReply}
						autofocus
					/>
				) }

			</div>
		);
	}
}




const PostMenu = ({ isMine, currentUser, post, user, onDelete }) => (
	<aside class="menu box">
		<ul class="menu-list">
			<li><a href={location.pathname+'?post='+post.id}>Open Full</a></li>
			<li><a href={`/profile/${encodeURIComponent(user.username)}`}>View Profile</a></li>
			{ isMine && ([
				<hr />,
				<li><a class="is-danger" onClick={onDelete}>Delete</a></li>
			]) }
			{/* { currentUser && ([
				<hr />,
				isMine ? ([
					<li><a class="is-danger" onClick={onDelete}>Delete</a></li>
				]) : ([
					<li><a class="is-danger is-disabled">Block</a></li>,
					<li><a class="is-danger is-disabled">Report</a></li>
				])
			]) } */}
		</ul>
	</aside>
);
