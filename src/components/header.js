import { Component } from 'preact';
import cx from 'clsx';
import { route } from 'preact-router';
import { connect } from 'unistore/preact';
import chime from '../lib/chime-connect';

@chime(null, 'refresh')
@connect('url,loading,loggedin,user')
export default class Header extends Component {
	toggle = () => {
		this.setState({ open: !this.state.open });
	};

	compose = () => {
		route(location.pathname+'?new');
	};

	render({ url='/', loading, loggedin, user={}, refresh }, { open }) {
		return (
			<nav class="nav has-shadow">
				<div class="nav-left">
					<Tab href="/" currentUrl={url} icon="home">
						Home
					</Tab>
					{ loggedin && ([
						<Tab href="/public" currentUrl={url} icon="feed">
							Public
						</Tab>,
						<Tab href="/notifications" currentUrl={url} icon="bell">
							Notifications
						</Tab>
					]) }
				</div>

				<div class="nav-center">
					<a class="nav-item" onClick={refresh}>
						<span class="icon">
							<i class={cx('fa', 'fa-adjust', loading && 'fa-spin')} />
						</span>
					</a>
				</div>

				{ loggedin ? ([
					<div class="nav-right">
						<span class="nav-item">
							<a class="button is-primary" onClick={this.compose}>
								<span class="icon is-marginless">
									<i class="fa fa-pencil" />
								</span>
								<span class="is-hidden-mobile" style="margin-left:.5em;">Post</span>
							</a>
						</span>
					</div>,
					<div class="nav-right">
						<span class="nav-item">
							<a href={'/profile/'+encodeURIComponent(user.username)}>
								<p class="image is-32x32 is-avatar" style={{ backgroundImage: `url('${user.avatar_url}')` }} />
							</a>
						</span>
					</div>
				]) : ([
					<div class="nav-right">
						<span class="nav-item">
							<a class="button" href="/register">
								Sign Up
							</a>
						</span>
					</div>,
					<div class="nav-right">
						<span class="nav-item">
							<a class="button is-primary" href="/login">
								Log In
							</a>
						</span>
					</div>
				]) }
			</nav>
		);
	}
}


const Tab = ({ href, currentUrl, icon, children }) => (
	<a href={href} class={cx('nav-item', 'is-tab', href===currentUrl && 'is-active')}>
		{ icon && (
			<span class="icon is-marginless">
				<i class={'fa fa-'+icon} />
			</span>
		) }
		{ icon ? <span class="is-hidden-mobile" style="margin-left:.5em;">{children}</span> : children }
	</a>
);
