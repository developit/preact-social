export default () => EMPTY_POST;

const EMPTY_POST = (
	<div class="post">
		<article class="media">
			<figure class="media-left">
				<a>
					<p class="image is-48x48 is-avatar" style={{ backgroundColor: '#CCC' }} />
				</a>
			</figure>
			<div class="media-content">
				<div class="content">
					<div>
						<a style="font-size:120%;">...</a>
					</div>
					<div class="post-text">
						...
					</div>
					<div>
						<button class="button is-inverted is-disabled">
							<span class="icon is-small">
								<i class="fa fa-heart" />
							</span>
							<span>0</span>
						</button>
						<button class="button is-inverted is-disabled">
							<span class="icon is-small">
								<i class="fa fa-reply" />
							</span>
							<span>0</span>
						</button>
						<a class="button is-link is-text">...</a>
					</div>
				</div>
			</div>
		</article>
	</div>
);
