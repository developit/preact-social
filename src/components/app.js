import { Component } from 'preact';
import { Router, route } from 'preact-router';
import Provider from '../lib/provider';
import Header from './header';
import ModalViewer from './modal-viewer-lazy';
// import screens from './screens';

import WelcomeScreen from './welcome';
import ErrorScreen from '../routes/error';
import HomeScreen from '../routes/home';
import LoginScreen from '../routes/login';
import NotificationsScreen from '../routes/notifications';
import ProfileScreen from '../routes/profile';
import PublicScreen from '../routes/public';
import SearchScreen from '../routes/search';

import createStore from '../lib/store';
import { connect } from 'unistore/preact';
import chimeClient from '../lib/chime';
import config from '../config';

let store = createStore({
	config
});

let chime = chimeClient({
	config,
	store
});

if (typeof window!=='undefined') {
	window.chime = chime;
}

@connect(['loggedin', 'authfailed', 'username', 'error'])
class AppRoot extends Component {
	handleUrlChange({ url }) {
		let prev = store.getState().url || '/';
		if (url===prev) return;
		store.setState({ url });
		if (typeof ga==='function') {
			ga('send', 'pageview', url);
		}
	}

	componentDidUpdate() {
		if (this.props.authfailed && this.props.url!=='/login') {
			route('/login');
		}
	}

	render({ loggedin, username }) {
		return (
			<div id="app">
				<Header />

				<ModalViewer />

				{ PRERENDER && <link rel="preconnect" href="https://chimeline.herokuapp.com" /> }

				<main>
					{ PRERENDER ? (
						<WelcomeScreen path="/" />
					) : (
						<Router onChange={this.handleUrlChange}>
							{ loggedin ? ([
								<HomeScreen path="/" />,
								<PublicScreen path="/public" />,
								<NotificationsScreen path="/notifications" />,
								<ProfileScreen path="/profile" user={username} />
							]) : ([
								<WelcomeScreen path="/" />,
								<LoginScreen path="/login" mode="login" />,
								<LoginScreen path="/register" mode="register" />
							]) }
							<ProfileScreen path="/profile/:user" />
							<ProfileScreen path="/profile/:user/:section" />
							<SearchScreen path="/search" />
							<ErrorScreen default />
						</Router>
					) }
				</main>

				{/* { error && (
					<Toast message={error.message || error} time={2} />
				) } */}
			</div>
		);
	}
}


export default () => (
	<Provider store={store} chime={chime}>
		<AppRoot />
	</Provider>
);
