import { Component } from 'preact';
import { connect } from 'unistore/preact';
import ModalViewer from 'async!./modal-viewer';

const DIALOG_REG = /([?&])(post|comment|new)(?:=(\w+))?(&|#|$)/;

@connect('url')
export default class LazyModalViewer extends Component {
	componentDidMount() {
		const preload = () => {
			if (this.hasRendered) return;
			this.hasRendered = true;
			this.setState({});
		}
		setTimeout(() => {
			if (typeof requestIdleCallback === 'function') requestIdleCallback(preload);
			else preload();
		}, 2000);
	}
	render({ url, ...props }) {
		if (url && DIALOG_REG.test(url)) {
			this.hasRendered = true;
		}
		return this.hasRendered ? <ModalViewer {...props} /> : null;
	}
}
