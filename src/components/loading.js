export default () => (
	<div class="loading has-text-centered">
		<span class="icon is-medium">
			<i class="fa fa-circle-o-notch fa-spin" />
		</span>
	</div>
);
