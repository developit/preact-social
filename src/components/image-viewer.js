import { h, cloneElement, Component } from 'preact';
import Portal from 'preact-portal';
import cx from 'clsx';

export default class ImageViewer extends Component {
	open = () => this.setState({ open: true });

	close = () => this.setState({ open: false });

	loaded = e => {
		let orientation = e.target.naturalHeight > e.target.naturalWidth ? 'portrait' : 'landscape';
		this.setState({ loaded: true, orientation });
	};

	componentDidUpdate(prevProps, prevState) {
		if (this.state.open!=prevState.open && typeof document!=='undefined') {
			document.documentElement.style.overflowY = this.state.open ? 'hidden' : '';
		}
	}

	render({ src, thumb, children }, { open, loaded, orientation }) {

		return (
			<a onClick={this.open} title="View image">
				{children}
				{ open && (
					<Portal into="body">
						<div class="modal is-active is-image-viewer">
							<div class="modal-background" onClick={this.close} />
							<div class={cx('modal-content', !loaded && 'is-loading', orientation && `orientation-${orientation}`)}>
								<div class="is-overlay" onClick={this.close} style={{
									backgroundImage: thumb ? `url(${thumb})` : null
								}} />
								<div class="is-overlay" onClick={this.close} style={{
									backgroundImage: `url(${src})`
								}} />
								<img src={src} onLoad={this.loaded} onError={this.loaded} />
								{ !loaded && (
									<span class="icon icon-large is-spinner">
										<i class="fa fa-circle-o-notch fa-spin" />
									</span>
								) }
							</div>
							<button class="modal-close" onClick={this.close} />
						</div>
					</Portal>
				) }
			</a>
		);
	}
}
