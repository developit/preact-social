import { Component } from 'preact';
import { route } from 'preact-router';
import { connect } from 'unistore/preact';
import { GetPost, GetPostForComment } from './post';
import Compose from '../routes/compose';


const DIALOG_REG = /([?&])(post|comment|new)(?:=(\w+))?(&|#|$)/;

function parse(url) {
	let m = url.match(DIALOG_REG);
	return m && {
		type: m[2],
		value: decodeURIComponent(m[3])
	};
}

@connect( state => ({
	item: parse(state.url || ''),
	url: state.url
}) )
export default class ModalViewer extends Component {
	close = () => {
		let { url } = this.props;
		route(url.replace(DIALOG_REG, (s, before, a, b, after) => `${after==='&' ? before : ''}${after==='#'?'#':''}`), true);
	};

	handleKey = e => {
		if (e.keyCode===27) {
			this.close();
		}
	};

	componentDidMount() {
		addEventListener('keydown', this.handleKey);
	}

	componentWillUnmount() {
		removeEventListener('keydown', this.handleKey);
	}

	getChild(item) {
		return item && item.type && TYPES[item.type.toLowerCase()];
	}

	componentDidUpdate({ item }) {
		let current = this.getChild(this.props.item);
		if (current!=this.getChild(item) && typeof document!=='undefined') {
			document.documentElement.style.overflowY = current ? 'hidden' : '';
		}
	}

	render({ item }) {
		let Child = this.getChild(item);

		return Child ? <Child item={item} onClose={this.close} /> : null;
	}
}

const Modal = ({ title, children, onClose }) => (
	<div class="modal is-active is-top">
		<div class="modal-background" onClick={onClose} />
		{ title ? (
			<div class="modal-card">
				<header class="modal-card-head">
					<p class="modal-card-title">{title}</p>
					<button class="delete" onClick={onClose} />
				</header>
				<section class="modal-card-body">
					{children}
				</section>
			</div>
		) : (
			<div class="modal-content">
				<div class="box">
					{children}
				</div>
			</div>
		) }
		{ !title && <button class="modal-close" onClick={onClose} /> }
	</div>
);

const TYPES = {
	post: ({ item, onClose }) => (
		<Modal onClose={onClose}>
			<GetPost id={item.value} isModal showComments />
		</Modal>
	),
	comment: ({ item, onClose }) => (
		<Modal onClose={onClose}>
			<GetPostForComment id={item.value} isModal />
		</Modal>
	),
	new: ({ item, onClose }) => (
		<Modal title="Compose Post" onClose={onClose}>
			<Compose modal onCancel={onClose} onAdd={onClose} />
		</Modal>
	)
};
