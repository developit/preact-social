import { Component } from 'preact';
import chime from '../lib/chime-connect';
import shallowEqual from '../lib/shallow-equal';
import Post from './post';
import Compose from '../routes/compose';
import Loading from './loading';
import ScrollHook from '../lib/scroll-hook';

const PER_PAGE = 20;

export default class Stream extends Component {
	state = {
		loading: true,
		pages: [null]
	};

	// skip updates for `loading` and `before` in state.
	shouldComponentUpdate(props, state) {
		if (!shallowEqual(props, this.props)) return true;
		for (let i in state) {
			if (i!=='loading' && i!=='before' && state[i]!==this.state[i]) return true;
		}
		for (let i in this.state) {
			if (i!=='loading' && i!=='before' && !(i in state)) return true;
		}
		return false;
	}

	loadMore() {
		let { pages, before } = this.state;
		this.setState({ loading: true, pages: pages.concat(before) });
	}

	onResults = results => {
		results = results || [];
		let count = results.length,
			last = count && results[count-1];

		if (last) this.setState({ before: last.id });

		this.setState({
			loading: false,
			done: count < PER_PAGE
		});
	};

	onScroll = ({ offset, max }) => {
		let { loading, done } = this.state;
		if (!loading && !done && offset > max - window.innerHeight/2) {
			this.loadMore();
		}
	};

	render({ showCompose, streamType, ...props }, { loading, done, pages, before }) {
		let pagesRendered = [];

		for (let i=0; i<pages.length; i++) {
			let last = i==pages.length-1;
			pagesRendered.push(
				<Page type={streamType} before={pages[i]} onLoad={last && this.onResults} />
			);
		}

		return (
			<ScrollHook onScroll={this.onScroll}>
				<div class={props.class}>
					{ showCompose && (
						<div class="box is-centered post-compose-top">
							<Compose cancellable={false} />
						</div>
					) }

					<div class="box is-centered posts">
						<div class="infinite-wrap">
							{ pagesRendered }
						</div>
						{ !done && <Loading /> }
					</div>
				</div>
			</ScrollHook>
		);
	}
}


@chime( ({ type, before }) => ({
	posts: [type || 'timeline', before && { query: { before_id: before } }]
}))
class Page extends Component {
	state = {
		before: this.props.before
	};

	componentDidMount() {
		let { posts, onLoad } = this.props;
		if (posts) onLoad(posts);
	}

	componentWillReceiveProps({ posts }) {
		if (posts && !this.props.posts && this.props.onLoad) {
			this.props.onLoad(posts);
		}
	}

	renderPost = post => (
		<Post post={post} />
	);

	render({ posts }, { before }) {
		return (
			<div class="infinite-page" data-before={before}>
				{ posts && posts.map(this.renderPost) }
			</div>
		);
	}
}
