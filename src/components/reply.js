import { Component } from 'preact';
import Compose from '../routes/compose';


export default class Reply extends Component {
	render(props) {
		return (
			props.modal ? (
				<ReplyModal {...props} />
			) : (
				<Compose type="comment" {...props} />
			)
		);
	}
}


const ReplyModal = props => (
	<div class="modal is-active is-top">
		<div class="modal-background" onClick={props.onCancel} />
		<div class="modal-card">
			<header class="modal-card-head">
				<p class="modal-card-title">Reply to @{props.post.user.username}</p>
				<button class="delete" onClick={props.onCancel} />
			</header>
			<section class="modal-card-body">
				<div class="content">
					<blockquote style="padding:10px;">
						<div>
							<a>{props.post.user.name}</a>
						</div>
						<p>{props.post.body}</p>
					</blockquote>
				</div>
				<Compose modal type="comment" {...props} />
			</section>
		</div>
	</div>
);
