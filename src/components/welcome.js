import Public from '../routes/public';

export default () => (
	<div class="public page">
		<section class="hero is-primary">
			<div class="hero-body">
				<div class="container">
					<h1 class="title">Preact Social</h1>
					<h2 class="subtitle">What's all this then?</h2>
					<p>This is a little social network similar to the birdsite.</p>
					<br />
					<p>It also happens to be a place where we test out experimental builds of Preact in semi-real-world usage.</p>
				</div>
			</div>
		</section>
		<Public />
	</div>
);
