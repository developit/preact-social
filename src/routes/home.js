import { Component } from 'preact';
import Stream from '../components/stream';

export default class Home extends Component {
	render() {
		return <Stream class="home page" streamType="timeline" showCompose />;
	}
}


// @chime({
// 	posts: 'timeline'
// })
// export default class Home extends Component {
// 	render({ posts }) {
// 		return (
// 			<div class="home page">
// 				<div class="box is-centered post-compose-top">
// 					<Compose cancellable={false} />
// 				</div>
//
// 				<div class="box is-centered posts">
// 					{ posts ? (
// 						posts.map( post => <Post post={post} /> )
// 					) : (
// 						<Loading />
// 					) }
// 				</div>
// 			</div>
// 		);
// 	}
// }
