import { Component } from 'preact';
import cx from 'clsx';
import { route } from 'preact-router';
import chime from '../lib/chime-connect';

@chime({
	loggedin: 'isLoggedIn'
}, chime => ({
	...chime.account,
	me: chime.user.me
}))
export default class Login extends Component {
	state = {
		register: this.props.mode==='register'
	};

	componentWillReceiveProps({ mode }) {
		if (mode!==this.props.mode && (mode==='register')!==this.state.mode) {
			this.setState({ register: mode==='register' });
		}
	}

	update = e => {
		let { name, value } = e.target;
		this.setState({ [name]: value });
	};

	toggleType = () => {
		this.setState({ register: !this.state.register, error: false, loading: false });
	};

	submit = () => {
		let { register, ...fields } = this.state;
		this.setState({ loading: true });
		this.props[register?'register':'authorize'](fields)
			.then( () => {
				this.setState({ loading: false, error: null });
				setTimeout( () => {
					this.props.me().then( () => route('/') );
				});
			})
			.catch( err => {
				this.setState({ loading: false, error: err.message });
			});
	};

	render({ }, { register, loading, error, name, username, email, password }) {
		return (
			<div class="login is-centered box" data-mode={register ? 'register' : 'login'}>
				<h1 class="title">{register ? 'Set up an account' : 'Log in to Preact Social'}</h1>
				{ error && (
					<p key="error" class="notification is-warning">
						{String(error)}
					</p>
				) }

				<form onSubmit={this.submit} action="javascript:">
					<label class="label">
						Username:
						<input class="input" name="username" pattern="^[a-z0-9_]{2,}$" value={username} onInput={this.update} />
					</label>
					<label class="label">
						Password:
						<input class="input" name="password" type="password" value={password} onInput={this.update} />
					</label>

					{ register && [
						<label class="label">
							Email:
							<input class="input" name="email" type="email" value={email} onInput={this.update} />
						</label>,
						<label class="label">
							Name:
							<input class="input" name="name" type="text" value={name} onInput={this.update} />
						</label>
					] }

					<p class="control" style="padding:10px 0 0;">
						<button class={cx('button', 'pull-right', 'is-primary', loading && 'is-loading')} type="submit">
							<span>{register ? 'Register' : 'Log In'}</span>
							<span class="icon">
								<i class="fa fa-sign-in" />
							</span>
						</button>

						<a class="button is-outline" href={register ? '/login' : '/register'}>
							{register ? 'Have an account? Log In.' : 'Create a new account'}
						</a>
					</p>
				</form>
			</div>
		);
	}
}
