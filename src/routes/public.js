import { Component } from 'preact';
import chime from '../lib/chime-connect';
import Stream from '../components/stream';

@chime({ isLoggedIn: 'isLoggedIn' })
export default class Public extends Component {
	render({ isLoggedIn }) {
		return (
			<Stream class="public page" streamType="publicTimeline" showCompose={isLoggedIn} />
		);
	}
}
