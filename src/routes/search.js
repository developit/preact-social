import { Component } from 'preact';
import chime from '../lib/chime-connect';
import Post from '../components/post';
import Loading from '../components/loading';

@chime({
	posts: ['search', 'q']
})
export default class Profile extends Component {
	render({ q, posts }, { isUnfollowing }) {
		return (
			<div class="search page">
				<section class="hero is-bold is-light">
					<div class="hero-body">
						<div class="container">
							<h1 class="title">{q}</h1>
							<h2 class="subtitle">Showing results for {q}</h2>
						</div>
					</div>
				</section>

				<div class="container">
					<div class="box posts">
						{ posts ? (
							posts.length ? (
								posts.map( post => (
									<Post post={post} />
								))
							) : (
								<div class="no-results has-text-centered">
									<h2 class="title is-4">No Results</h2>
									<h3 class="subtitle is-6">Search for {q} returned no results.</h3>
								</div>
							)
						) : (
							<Loading />
						) }
					</div>
				</div>
			</div>
		);
	}
}
