import { Component } from 'preact';
import cx from 'clsx';
import linkState from 'linkstate';
import chime from '../lib/chime-connect';
import contrast from 'contrast';
// import { shade } from 'color-helpers';
// import rgbToHex from 'rgb-hex';
// import hexToRgb from 'hex-rgb';
import Post from '../components/post';
import Loading from '../components/loading';

const PROFILE_FIELDS = ['name', 'location', 'bio', 'website', 'color'];
function copyProfile(onto, from) {
	for (let i=0; i<PROFILE_FIELDS.length; i++) {
		onto[PROFILE_FIELDS[i]] = from[PROFILE_FIELDS[i]];
	}
	return onto;
}

@chime({
	currentUser: ['currentUser', 'user'],
	profile: ['user.profile', 'user']
}, chime => ({
	onFollow: chime.user.follow,
	onUnfollow: chime.user.unfollow,
	onUpdateProfile: chime.account.updateProfile
}))
export default class Profile extends Component {
	componentWillReceiveProps({ user, profile }) {
		if (user!==this.props.user) {
			this.setState({ loading: true });
		}
		else if (profile) {
			this.setState({ loading: false });
		}
	}

	follow = () => {
		let { onFollow, profile } = this.props;
		this.setState({ following: true });
		onFollow(profile.username);
	};

	unfollow = () => {
		if (!this.state.isUnfollowing) {
			return this.setState({ isUnfollowing: true });
		}
		let { onUnfollow, profile } = this.props;
		this.setState({ following: false });
		onUnfollow(profile.username);
	};

	cancelUnfollow = () => {
		this.setState({ isUnfollowing: false });
	};

	toggleEdit = () => {
		let { editMode, profile } = this.state;
		editMode = !editMode;
		this.setState({
			editMode,
			profile: editMode ? { ...this.props.profile } : null
		});
	};

	saveEdit = () => {
		let profile = copyProfile({}, this.state.profile);
		this.props.onUpdateProfile(profile).then( () => {
			copyProfile(this.props.profile, profile);
			this.setState({ editMode: false, profile: null });
		});
	};

	setColor = e => {
		let { profile } = this.state;
		profile.color = e.target.value.replace(/^#/,'');
		this.setState({ profile });
	};

	render({ user, currentUser, profile, section='posts' }, { isUnfollowing, following, editMode, profile: editProfile }) {
		if (!user && currentUser) user = currentUser.username;

		let hasProfile = !!profile;

		if (!profile) {
			profile = {
				name: user,
				username: user
			};
		}
		if (editMode) {
			profile = editProfile;
		}

		let color = `#${profile && profile.color || 'cccccc'}`,
			mode = contrast(color),
			// altColor = '#' + shade(color.substring(1), mode==='light' ? -.5 : .5),
			antiMode = mode==='dark' ? 'light' : 'dark';

		let isSelf = user==='me' || (profile && currentUser && profile.username===currentUser.username);

		let urlPrefix = '/profile/'+user;

		if (following==null && profile) following = profile.is_followed;

		let Section = SECTIONS[section] || SECTIONS.posts;

		return (
			<div class={cx('profile page', (!hasProfile || this.state.loading) && 'loading')}>
				<section class={cx('hero', 'is-'+mode)} style={{
					backgroundColor: color,
					// backgroundImage: `linear-gradient(140deg, rgba(${mode==='dark'?'255,255,255':'0,0,0'},0) 0%, rgba(${mode==='dark'?'255,255,255':'0,0,0'},0.5) 100%)`
					backgroundImage: `linear-gradient(140deg, rgba(255,255,255,0.25) 0%, rgba(0,0,0,0.25) 100%)`
					// backgroundImage: `linear-gradient(140deg, ${color} 0%, ${altColor} 100%)`
					// backgroundImage: `linear-gradient(140deg, ${color} 0%, ${contrast(color)==='light'?'#FFF':'#000'} 100%)`
				}}>
					<div class="hero-body">
						<div class="container">
							<div class="pull-left">
								<p class="image is-avatar is-96x96">
									{ profile && profile.avatar_url && (
										<img src={profile.avatar_url} />
									) }
								</p>
							</div>

							<h1 class="title">{profile && profile.name || '...'}</h1>
							<h2 class="subtitle">@{profile && profile.username || user}</h2>

							<div class="pull-right">
								{ isSelf ? (
									<span>
										{ editMode ? (
											<button class="button is-danger is-outlined" onClick={this.toggleEdit}>Cancel</button>
										) : (
											<button class={'button is-outlined is-disabled is-'+antiMode}>You</button>
										) }
										{' '}
										<button class={cx('button is-primary', !editMode && 'is-inverted is-outlined')} onClick={editMode?this.saveEdit:this.toggleEdit}>
											<span class="icon is-small">
												<i class={'fa fa-' + (editMode?'save':'pencil')} />
											</span>
											<span>{editMode?'Save Profile':'Edit Profile'}</span>
										</button>
									</span>
								) : currentUser && (
									following ? (
										isUnfollowing ? (
											<span>
												<button class="button is-danger" onClick={this.unfollow}>
													<span class="icon is-small">
														<i class="fa fa-remove" />
													</span>
													<span>Unfollow</span>
												</button>
												{' '}
												<button class={'button is-outlined is-'+antiMode} onClick={this.cancelUnfollow}>
													<span>Cancel</span>
												</button>
											</span>
										) : (
											<button class={'button is-outlined is-'+antiMode} onClick={this.unfollow}>
												<span class="icon is-small">
													<i class="fa fa-check" />
												</span>
												<span>Following</span>
											</button>
										)
									) : (
										<button class="button is-primary is-outlined" onClick={this.follow}>
											<span class="icon is-small">
												<i class="fa fa-plus" />
											</span>
											<span>Follow</span>
										</button>
									)
								) }
							</div>
						</div>
					</div>

					<div class="hero-foot">
						<nav class="tabs is-centered is-boxed">
							<ul>
								<Tab href={urlPrefix} current={section==='posts'}>Posts</Tab>
								<Tab href={urlPrefix+'/comments'} current={section==='comments'}>Comments</Tab>
								<Tab href={urlPrefix+'/followers'} current={section==='followers'}>Followers</Tab>
								<Tab href={urlPrefix+'/following'} current={section==='following'}>Following</Tab>
							</ul>
						</nav>
					</div>
				</section>

				<div class="container">
					<div class="columns">
						<div class="column is-narrow is-hidden-mobile">
							<div class="sidebar">
								<h2 class="title is-5">
									<InlineEdit
										edit={editMode}
										value={profile && profile.name || user}
										placeholder="Your Name"
										onInput={linkState(this, 'profile.name')}
									/>
								</h2>
								<h3 class="subtitle is-6">
									<a href={`/profile/${encodeURIComponent(profile.username)}`}>@{profile.username}</a>
								</h3>
								<p class="bio">
									<InlineEdit
										edit={editMode}
										element="textarea"
										value={profile && profile.bio || ''}
										placeholder="A short biography"
										onInput={linkState(this, 'profile.bio')}
									/>
								</p>
								{ editMode && (
									<div class="has-space-above">
										<label class="select is-fullwidth is-color-picker">
											<select readonly><option>Profile Color</option></select>
											<span class="color-preview" style={{ backgroundColor: color }} />
											<input type="color" value={color} onInput={this.setColor} />
										</label>
									</div>
								) }
							</div>
						</div>

						<div class="column">
							{ hasProfile ? (
								<Section user={user} profile={profile} />
							) : (
								<Loading />
							) }
						</div>
					</div>
				</div>
			</div>
		);
	}
}



const SECTIONS = {
	posts: chime({ posts: ['user.posts', 'user'] })( props => <PostsList {...props} type="post" /> ),
	comments: chime({ posts: ['user.comments', 'user'] })( props => <PostsList {...props} type="comment" /> ),
	followers: chime({ users: ['user.followers', 'user'] })( props => <UsersList {...props} type="follower" /> ),
	following: chime({ users: ['user.following', 'user'] })( props => <UsersList {...props} type="following" /> )
};


class PostsList extends Component {
	render({ posts, user, profile, type }) {
		return (
			<div class="box posts">
				{ posts ? (
					posts && posts.length ? (
						posts.map( post => (
							<Post post={post} user={profile} />
						))
					) : (
						<div class="has-text-centered">
							<h2 class="title is-4">No {type}s</h2>
							<h3 class="subtitle is-6">@{user} has no {type}s.</h3>
						</div>
					)
				) : (
					<Loading />
				) }
			</div>
		);
	}
}


class UsersList extends Component {
	render({ users, user, type }) {
		return (
			<div class="users">
				{ users ? (
					users.length ? (
						users.map( user => (
							<User user={user} />
						))
					) : (
						<div class="has-text-centered">
							<h2 class="title is-4">Nobody Home</h2>
							<h3 class="subtitle is-6">@{user} {type==='followers'?'has no followers':'is not following anyone'}.</h3>
						</div>
					)
				) : (
					<Loading />
				) }
			</div>
		);
	}
}


class User {
	render({ user }) {
		let url = `/profile/${encodeURIComponent(user.username)}`;
		return (
			<div class="user">
				<div class="box">
					<a class="image is-48x48 is-avatar is-pulled-left" href={url}>
						<img src={user.avatar_url} />
					</a>
					<h3 class="title is-5">{user.name || user.username}</h3>
					<h4 class="subtitle is-6">
						<a href={url}>@{user.username}</a>
					</h4>
					<p class="bio">{user.bio}</p>
				</div>
			</div>
		);
	}
}


const InlineEdit = ({ edit, element: Type='input', value, ...props }) => (
	edit ? (
		<p class={cx('control is-inline-edit is-editing', props.class)}>
			<Type {...props} class={Type} value={value} />
		</p>
	) : (
		<span class={cx('is-inline-edit', props.class)}>{value}</span>
	)
);



const Tab = ({ href, current, children }) => (
	<li class={cx('is-tab', current && 'is-active')}>
		<a href={href}>{children}</a>
	</li>
);
