export default () => (
	<div class="page error-page">
		<div class="box is-centered">
			<h1 class="title">Not found</h1>
		</div>
	</div>
);
