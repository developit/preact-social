import { Component } from 'preact';
import canvasToBlob from '../lib/canvas-to-blob';
import chooseFiles from 'choose-files';
import cx from 'clsx';
import linkState from 'linkstate';
import chime from '../lib/chime-connect';
import ImageViewer from '../components/image-viewer';

const MAX_IMAGE_SIZE = 1000;

@chime({
	user: 'currentUser'
}, chime => ({
	submitPost: chime.posts.create,
	submitComment: chime.comments.create,
	uploadImage: chime.uploadImage,
	deleteImage: chime.deleteImage
}))
export default class Compose extends Component {
	static defaultProps = {
		type: 'post'
	};

	state = {
		body: this.getInitialMessage(this.props.post),
		images: []
	};

	getInitialMessage(post) {
		if (!post) return '';
		let mentions = [post.user.username].concat(post.entities.user_mentions.map( p => p.username || p.screen_name ));
		mentions = mentions.filter( n => n !== this.props.user.username );
		return mentions.map( m => '@'+m ).join(' ') + ' ';
	}

	getAvailableLength() {
		return 200 - this.state.images.reduce( (acc, img) => acc + img.url.length + 1, 0);
	}

	inputRef = c => {
		this.input = c;
	};

	handleKey = e => {
		let prevent = false;
		if (e.keyCode===13 && (e.ctrlKey || e.metaKey)) {
			this.post();
			prevent = true;
		}
		else if (e.keyCode===27) {
			if (this.props.onCancel) {
				this.props.onCancel();
			}
			else {
				this.input.blur();
			}
			prevent = true;
		}
		if (prevent) {
			if (e.stopPropagation) e.stopPropagation();
			e.preventDefault();
			return false;
		}
	};

	post = () => {
		let { type, submitPost, submitComment, post, onAdd } = this.props,
			{ body, images } = this.state;
		if (images && images.length) {
			body += '\n' + images.map( img => img.url ).join('\n');
		}
		let submitted = { body };
		this.setState({ sending: true });
		(type==='comment' ? submitComment(post.id, submitted) : submitPost(submitted)).then( () => {
			this.setState({ sending: false, body: '', images: [] });
			if (onAdd) onAdd(submitted);
		});
	};

	addImage = () => {
		chooseFiles( files => {
			if (!files || !files.length) return;
			this.setState({ uploading:true });

			Promise.all(files.map(this.uploadImage))
				.then( images => {
					images = this.state.images.concat(images.filter(Boolean));
					this.setState({ uploading:false, error:null, images });
				}).catch( error => {
					this.setState({ error });
				});
		});
	};

	removeImage = img => {
		this.setState({
			images: this.state.images.filter( ({ id }) => id!==img.id )
		});
		this.props.deleteImage(img);
	};

	uploadImage = img => Promise.resolve(img)
		.then(this.resizeImage)
		.then(this.props.uploadImage);

	resizeImage = img => new Promise( (resolve, reject) => {
		let image = new Image(),
			url = URL.createObjectURL(img);
		setTimeout( () => URL.revokeObjectURL(url), 1000);
		image.src = url;
		image.onload = () => {
			let { width, height } = image;
			if (width>MAX_IMAGE_SIZE || height>MAX_IMAGE_SIZE) {
				let scale = MAX_IMAGE_SIZE / Math.max(width, height),
					canvas = document.createElement('canvas');
				canvas.width = width * scale;
				canvas.height = height * scale;
				let ctx = canvas.getContext('2d');
				ctx.drawImage(image, 0, 0, canvas.width, canvas.height);
				canvasToBlob(canvas, resolve, 'image/jpeg', 0.9);
			}
			else {
				resolve(img);
			}
		};
		image.onerror = () => reject('Not a valid image');
	});

	componentDidMount() {
		if (this.props.modal===true || this.props.autofocus===true) {
			this.base.querySelector('textarea').focus();
			if (this.base.scrollIntoView) {
				this.base.scrollIntoView();
			}
		}
	}

	render({ post, user, cancellable, modal, onCancel }, { body, uploading, sending, images=[] }) {
		let chars = body && body.length || 0,
			maxLength = this.getAvailableLength();
		return (
			<article class={cx('compose post is-new is-edit media', modal && 'is-modal')}>
				<figure class="media-left">
					<p class="image is-48x48 is-avatar" style={{ backgroundImage: `url('${user.avatar_url}')` }} />
				</figure>
				<div class="media-content">
					<p class="control">
						<textarea
							class="textarea is-post-editor"
							placeholder={post ? 'Add a comment...' : 'Type a message...'}
							value={body}
							onInput={linkState(this, 'body')}
							onKeyDown={this.handleKey}
							style={{ height: Math.min(12, 3 + body.split('\n').length)*1.21 + 'em' }}
							ref={this.inputRef}
						/>
						<span class={cx('char-count', chars>maxLength ? 'is-over' : chars>(maxLength-40) && 'is-close')}>{chars ? (maxLength-chars) : ''}</span>
					</p>
					<p class="control block button-bar">
						<button class={cx(
							'button is-primary is-pulled-right',
							((!body && !images.length) || chars>maxLength || uploading || sending) && 'is-disabled',
							sending && 'is-loading'
						)} onClick={!uploading && !sending && this.post}>
							<span class="icon is-small">
								<i class="fa fa-send" />
							</span>
							<span>{post ? 'Reply' : 'Post'}</span>
						</button>

						{ cancellable!==false && (
							<button key="cancel" class={cx('button post-cancel is-link is-text is-light is-pulled-right', sending && 'is-disabled')} onClick={onCancel}>
								Cancel
							</button>
						) }

						<div class="compose-images">
							{ images && images.map( img => (
								<PostImageThumb img={img} onDelete={this.removeImage} />
							)) }
							<button class={cx('button', uploading && 'is-disabled is-loading')} onClick={this.addImage}>
								<span class="icon">
									<i class="fa fa-camera" />
								</span>
							</button>
						</div>
					</p>
				</div>
			</article>
		);
	}
}


class PostImageThumb extends Component {
	delete = () => {
		let { img, onDelete } = this.props;
		onDelete(img);
	};

	render({ img }) {
		let thumb = img.url.replace(/(\.[a-z]+)$/, 'b$1');
		return (
			<p class="image is-48x48">
				<ImageViewer src={img.url}>
					<img src={thumb} />
				</ImageViewer>
				<button class="delete" onClick={this.delete} />
			</p>
		);
	}
}
