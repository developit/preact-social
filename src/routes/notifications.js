import { Component } from 'preact';
import chime from '../lib/chime-connect';
import Loading from '../components/loading';
import RelativeTime from '../lib/relative-time';

@chime({
	notifications: 'notifications'
})
export default class Notifications extends Component {
	renderNotification(notification) {
		const Notify = NOTIFICATIONS[notification.notification_type];
		return <Notify notification={notification} />;
	}

	render({ notifications }) {
		return (
			<div class="notifications page">
				<div class="box is-centered posts">
					{ notifications ? (
						notifications.length ? (
							notifications.map(this.renderNotification)
						) : (
							<div class="no-results has-text-centered">
								<h2 class="title is-4">No Notifications</h2>
								<h3 class="subtitle is-6">Stir things up, get people talking.</h3>
							</div>
						)
					) : (
						<Loading />
					) }
				</div>
			</div>
		);
	}
}

// const NotificationObject = ({ notification }) => (
// 	notification.object_type==='comment' ? (
// 		<GetComment id={notification.object_id} />
// 	) : (
// 		<GetPost id={notification.object_id} />
// 	)
// );

const enc = encodeURIComponent;

const Action = ({ notification, icon, children }) => (
	<article class="media is-notification">
		<figure class="media-left">
			<a href={'/profile/'+encodeURIComponent(notification.user.username)}>
				<p class="image is-24x24 is-avatar" style={{
					backgroundImage: `url('${notification.user.avatar_url}')`
				}} />
			</a>
		</figure>
		<div class="media-content">
			<span class="icon icon-small is-badge">
				<i class={'fa fa-'+icon} />
			</span>
			<a href={`/profile/${enc(notification.user.username)}`}>
				{notification.user.name}
			</a>
			{' '}
			{children}
		</div>
	</article>
);

const NOTIFICATIONS = {
	like: ({ notification }) => (
		<Action icon="heart-o" notification={notification}>
			{'liked your '}
			<a href={location.pathname+`?${enc(notification.object_type)}=${enc(notification.object_id)}`}>
				{notification.object_type}
			</a> <RelativeTime time={notification.created_at} />.
		</Action>
	),

	comment: ({ notification }) => (
		<Action icon="comments-o" notification={notification}>
			{notification.object_type==='post' ? ' replied to your ' : ' commented on your '}
			<a href={location.pathname+`?${enc(notification.object_type)}=${enc(notification.object_id)}`}>
				{notification.object_type}
			</a> <RelativeTime time={notification.created_at} />.
		</Action>
	),

	follow: ({ notification }) => (
		<Action icon="user-plus" notification={notification}>
			followed you <RelativeTime time={notification.created_at} />.
		</Action>
	)
};
